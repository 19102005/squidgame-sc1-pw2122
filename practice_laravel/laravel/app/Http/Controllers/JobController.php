<?php

namespace App\Http\Controllers;

use App\Job;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class JobController extends Controller
{
    public function index()
    {
        $jobs = DB::table('jobs')->paginate(5);
        return view('admin.job.index', ['jobs' => $jobs]);
    }

    public function create()
    {
        return view('admin.job.create');
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "namaperusahaan" => "required",
            "posisi" => "required",
            "descjob" => "required",
            "lokasi" => "required",
            "gaji" => "required",
            "image" => "required"
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $job               = new Job();
        $image              = $request->file('image');
        if ($image) {
            $image_path     = $image->store('images', 'public');
            $job->image    = $image_path;
        }
        $job->namaperusahaan = $request->namaperusahaan;
        $job->posisi = $request->posisi;
        $job->descjob = $request->descjob;
        $job->lokasi = $request->lokasi;
        $job->gaji = $request->gaji;
        $job->image = $request->image;

        $job->save();
        return redirect()->route('job')->with('success', 'Data added successfully');
    }

    public function edit($id) {
        $job= Job::findOrFail($id);

        return view('admin.job.edit', [
            'job' => $job
        ]);
    }

    public function update(Request $request, $id) {
        $validator = Validator::make($request->all(), [
            "namaperusahaan" => "required",
            "posisi" => "required",
            "descjob" => "required",
            "lokasi" => "required",
            "gaji" => "required",
            "image" => "required"
        ]);
        
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
       
        $job = Job::find($id);
        $new_image = $request->file('image');

        if ($new_image) {
            if ($job->image && file_exists(storage_path('app/public/' . $job->image))) {
                Storage::delete('public/' . $job->image);
            }

            $new_image_path = $new_image->store('images', 'public');

            $job->image = $new_image_path;
        }

        $job->namaperusahaan = $request->namaperusahaan;
        $job->posisi = $request->posisi;
        $job->descjob = $request->descjob;
        $job->lokasi = $request->lokasi;
        $job->gaji = $request->gaji;
        $job->image = $request->image;

        $job->update();
        return redirect()->route('index')->with('success', 'Data added successfully');
    }
    public function delete($id)
    {
        $job= Job::find($id);
        $job->delete();

        return redirect()->route('index')->with('success', 'Data added successfully');
    }
}
