<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\view;

class FrontendController extends Controller
{
    public function __construct()
    {

        $jobs = DB::table('jobs')->paginate(6);

        view()->share([
            'jobs' => $jobs,
        ]);
    }
    public function index()
    {
        return view('index');
    }
    public function lowongan(){
        if (Auth::check()){
            return View::make('lowongan')->with('user', Auth::user());
        }
        else { return route('login'); }
    }
    public function keuangan() {
        return view('keuangan');
    }
    public function accounting() {
        return view('accounting');
    }
    public function bisnis() {
        return view('bisnis');
    }
    public function digitalmarketing() {
        return view('digitalmarketing');
    }
    public function digitalmarkertingofficer() {
        return view('digitalmarketingofficer');
    }
    public function faq() {
        return view('faq');
    }
    public function headofpromotion() {
        return view('headofpromotion');
    }
    public function layanan() {
        return view('layanan');
    }
    public function lembagahukum() {
        return view('lembagahukum');
    }
    public function managemen() {
        return view('managemen');
    }
    public function marketingcommunication() {
        return view('marketingcommunication');
    }
    public function marketingmanager() {
        return view('marketingmanager');
    }
    public function pemasaran() {
        return view('pemasaran');
    }
    public function pengembangan() {
        return view('pengembangan');
    }
    public function profil_keahlian() {
        return view('profil_keahlian');
    }
    public function profil_pendidikan() {
        return view('profil_pendidikan');
    }
    public function profil_pengalaman() {
        return view('profil_pengalaman');
    }
    public function profil_sertifikat() {
        return view('profil_sertifikat');
    }
    public function profil() {
        return view('profil');
    }
    public function salessupervisor() {
        return view('salessupervisor');
    }
    public function sdm() {
        return view('sdm');
    }
    public function seniorbrandmanager() {
        return view('seniorbrandmanager');
    }
    public function strategicpartners() {
        return view('strategicpartnership');
    }
    public function teamkami() {
        return view('teamkami');
    }
    public function home() {
        return view('home');
    }
    public function login() {
        return view('login');
    }
    
}
