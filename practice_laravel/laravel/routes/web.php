<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\FrontendController;
use App\Http\Controllers\JobController;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Auth\ForgotPasswordController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

#Route::get('/', function () {
   # return view('welcome');
#});

#Route::get('index', 'TestController@index');
Route::get('/', 'FrontendController@index');
Route::get('login', 'FrontendController@login');
Route::get('home', 'FrontendController@home');
Route::get('lowongan', 'FrontendController@lowongan');
Route::get('keuangan', 'FrontendController@keuangan');
Route::get('accounting', 'FrontendController@accounting');
Route::get('bisnis', 'FrontendController@bisnis');
Route::get('digitalmarketing', 'FrontendController@digitalmarketing');
Route::get('digitalmarketingofficer', 'FrontendController@digitalmarketingofficer');
Route::get('faq', 'FrontendController@faq');
Route::get('headofpromotion', 'FrontendController@headofpromotion');
Route::get('layanan', 'FrontendController@layanan');
Route::get('lembagahukum', 'FrontendController@lembagahukum');
Route::get('managemen', 'FrontendController@managemen');
Route::get('marketingcommunication', 'FrontendController@marketingcommunication');
Route::get('marketingmanager', 'FrontendController@marketingmanager');
Route::get('pemasaran', 'FrontendController@pemasaran');
Route::get('pengembangan', 'FrontendController@pengembangan');
Route::get('profil_keahlian', 'FrontendController@profil_keahlian');
Route::get('profil_pendidikan', 'FrontendController@profil_pendidikan');
Route::get('profil_pengalaman', 'FrontendController@profil_pengalaman');
Route::get('profil_sertifikat', 'FrontendController@profil_sertifikat');
Route::get('profil', 'FrontendController@profil');
Route::get('salessupervisor', 'FrontendController@salessupervisor');
Route::get('sdm', 'FrontendController@sdm');
Route::get('seniorbrandmanager', 'FrontendController@seniorbrandmanager');
Route::get('strategicpartnership', 'FrontendController@strategicpartnership');
Route::get('teamkami', 'FrontendController@teamkami');

Route::get('forget-password', [ForgotPasswordController::class, 'showForgetPasswordForm'])->name('forget.password.get');
Route::post('forget-password', [ForgotPasswordController::class, 'submitForgetPasswordForm'])->name('forget.password.post'); 
Route::get('reset-password/{token}', [ForgotPasswordController::class, 'showResetPasswordForm'])->name('reset.password.get');
Route::post('reset-password', [ForgotPasswordController::class, 'submitResetPasswordForm'])->name('reset.password.post');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/admin', 'AdminController@admin')->name('admin');
Route::get('/profile', 'AdminController@profileedit')->name('profile.edit');

Route::group(['prefix' => 'admin',  'middleware' => 'auth'], function () {
	//dashboard
	Route::get('dashboard', [AdminController::class, 'admin'])->middleware('checkRole:admin');
    Route::get('/home', 'HomeController@index' )->middleware(['checkRole:user,admin']);
	//Post Data
    Route::resource('/post', PostController::class);

    Route::get('/user_index', [AdminController::class, 'userindex'])->name('user.index');
    Route::get('/icons', [AdminController::class, 'icons'])->name('icons');
    Route::get('/map', [AdminController::class, 'map'])->name('map');
    Route::get('/table', [AdminController::class, 'table'])->name('table');
    Route::get('/job', [JobController::class, 'index'])->name('index');
    Route::get('/job/create', [JobController::class, 'create'])->middleware('checkRole:admin')->name('job.create');
    Route::post('/job/create', [JobController::class, 'store'])->name('job.store');
    Route::get('/job/edit/{id}', [JobController::class, 'edit'])->name('job.edit');
    Route::put('/job/edit/{id}', [JobController::class, 'update'])->name('job.update');
    Route::delete('/job/delete/{id}', [JobController::class, 'delete'])->name('job.delete');

});
