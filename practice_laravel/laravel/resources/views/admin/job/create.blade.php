@extends('layouts.app')

@section('content')
<div class="container">
    <form action="{{ route('job.store') }}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="form-row">
          <div class="form-group col-md-6">
            <label for="namaPerusahaan">Nama Perusahaan</label>
            <input type="text" class="form-control" id="namaPerusahaan" placeholder="Nama Perusahaan" name="namaperusahaan">
          </div>
        </div>
        <div class="form-group">
          <label for="posisi">Posisi</label>
          <input type="text" class="form-control" id="posisi" placeholder="Manager" name="posisi">
        </div>
        <div class="form-group">
          <label for="deskripsi">Deskripsi</label>
          <textarea type="text-area" class="form-control" id="deskripsi" placeholder="Pekerjaan ini adalah" name="descjob"></textarea>
        </div>
        <div class="form-row">
          <div class="form-group col-md-6">
            <label for="gaji">gaji</label>
            <input type="text" class="form-control" id="gaji" name="gaji">
          </div>
          <div class="form-group col-md-4">
            <label for="lokasi">lokasi</label>
            <input type="text" id="lokasi" class="form-control" name="lokasi">
          </div>
          <div class="form-group col-md-2">
            <label for="image">image</label>
            <input type="file" class="form-control" id="image" name="image">
          </div>
        </div>
        <button type="submit" class="btn btn-primary">Tambahkan</button>
    </form>
</div>

  

  @endsection

@push('js')
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.extension.js"></script>
@endpush