@extends('admin.layouts.app')

@section('content')
<div class="container">
    <form method="POST" action="{{ route('job.update',$job->id) }}" enctype="multipart/form-data">
    @csrf
    @method('PUT')
        <div class="form-row">
          <div class="form-group col-md-6">
            <label for="namaPerusahaan">Nama Perusahaan</label>
            <input type="text" class="form-control" id="namaPerusahaan" value="{{ $job->namaperusahaan }}" placeholder="Nama Perusahaan" name="namaperusahaan">
          </div>
        </div>
        <div class="form-group">
          <label for="posisi">Posisi</label>
          <input type="text" class="form-control" id="posisi" value="{{ $job->posisi }}" placeholder="Manager" name="posisi">
        </div>
        <div class="form-group">
          <label for="deskripsi">Deskripsi</label>
          <textarea type="text-area" class="form-control" {{ $job->description }} id="deskripsi" placeholder="Pekerjaan ini adalah" name="descjob"></textarea>
        </div>
        <div class="form-row">
          <div class="form-group col-md-6">
            <label for="gaji">gaji</label>
            <input type="text" class="form-control" id="gaji" value="{{ $job->gaji }}" name="gaji">
          </div>
          <div class="form-group col-md-4">
            <label for="lokasi">lokasi</label>
            <input type="text" id="lokasi" class="form-control" value="{{ $job->lokasi }}" name="lokasi">
          </div>
          <div class="form-group col-md-2">
            <div class="mx-auto pb-2">
                <img src="{{ asset('/storage') }}/{{ $job->image }}" id="output" width="100%" height="200px">
            </div>
            <label for="image">image</label>
            <input type="file" accept="image/*" class="form-control" name="image" id="file" onchange="loadFile(event)" value="{{ $job->image }}" style="display: none;">
            <label for="file" style="cursor: pointer;" class="btn"> 
                <span class="pl-1">Tambah Gambar</span>
            </label>
        </div>
        <button type="submit" class="btn btn-primary">Tambahkan</button>
    </form>
</div>

<script>
    var loadFile = function(event){
        var image = document.getElementById('output');
        image.src = URL.createObjectURL(event.target.files[0]);
    };
</script>

  @endsection

@push('js')
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.extension.js"></script>
@endpush