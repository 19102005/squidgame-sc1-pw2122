@extends('admin.layouts.app')

@section('content')
<div class="app-title">
    <div>
      <h1><i class="fa fa-th-list"></i> Data Perusahaan</h1>
      <p>Online Job Vacancy</p>
    </div>
    <a class="btn btn-primary" href="{{ route('job.create')}}"><i class="fa fa-plus"></i>Tambah Perusahaan</a>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="tile">
        <div class="tile-body">
          <table class="table table-hover table-bordered" id="sampleTable">
            <thead>
              <tr>
                <th>No</th>
                <th>Perusahaan</th>
                <th>Profil</th>
                <th>Alamat</th>
                <th>Email</th>
                <th>Logo</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
                @forelse ($jobs as $job)
                <tr>
                    <td><span>{{ $job->id }}</span></td>
                    <td><span>{{ $job->namaperusahaan }}</span></td>
                    <td><span>{{ $job->posisi }}</span></td>
                    <td><span>{{ $job->descjob }}</span></td>
                    <td><span>{{ $job->gaji }}</span></td>
                    <td>
                        <img src="{{ url('../laravel/public/storage/images') }}/{{ $job->image }}" width="100%" height="200px">
                    </td>
                    <td>
                        <a href="{{ route('job.edit',$job->id,'edit')}}">Edit</a>
                        <form action="{{ route('job.delete', $job->id)}}" method="post"> @method('DELETE') @csrf <button type="submit" >Delete</button>
                    </td>
                </tr>
                        @empty
                            <strong>data kosong</strong>
                @endforelse
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>

        @include('admin.layouts.footers.auth')
@endsection

@push('js')
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.extension.js"></script>
@endpush