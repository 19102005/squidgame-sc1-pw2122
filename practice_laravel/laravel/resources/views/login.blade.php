<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="{{ asset('assets2/img/icon.png')}}">
    <title>Masuk</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Comfortaa&family=Outfit&family=Roboto:wght@500&display=swap" rel="stylesheet">
  </head>

<style>
  .bg-nav{
    background-image: url("{{ asset('assets2/img/Rectangle.png')}}");
  }
  
</style>

<body>
  <!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-light bg-secondary fixed-top " style="min-height: 50px;">
  <div class="container-fluid" style="padding: 15px 50px;">
    <a class="navbar-brand" href="#" style="color: white;">
      <img src="{{ asset('assets2/img/icon.png')}}" alt="..." height="36"> Cari Kerja
    </a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="nav ms-auto nav-pills">
        <li class="nav-item">
          <a class="nav-link" aria-current="page" style="color: white;" href="index.html">Utama</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="lowongan.html" aria-current="page" style="color: white;">Lowongan</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="teamkami.html" aria-current="page" style="color: white;">Tentang</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="faq.html" aria-current="page" style="color: white;">Bantuan</a>
        </li>
        <li class="nav-item">
          <a class="nav-link active" href="login.html" aria-current="page" style="color: white;">Masuk</a>
        </li>
      </ul>
    </div>
  </div>
</nav>
  <!-- Navigation -->

  <!-- Page 1 -->
  <!--
    <div class="container" style="margin-top: 150px; margin-bottom: 50px;  padding-left: 150px; padding-right: 150px;">
        <div class="row">
        <div class="col">
            <img src="assets/img/avatar.png" alt="" style="object-fit: cover; width: 100%;">
        </div>
        <div class="col" style="margin-top:200px;">
            <h2 class="display-6" style="text-align: center; margin-bottom: 30px; margin-left: 75px;">Masuk</h2>
          <form>
            <div class="mb-3 row">
                <label for="exampleFormControlInput1" class="col-sm-2 col-form-label">Email</label>
                <div class="col-sm-10">
                    <input type="email" class="form-control" id="exampleFormControlInput1" placeholder="nama@domain.com" required>
                </div>
            </div>
            <div class="mb-3 row">
                <label for="inputPassword" class="col-sm-2 col-form-label">Kata Sandi</label>
                <div class="col-sm-10">
                  <input type="password" class="form-control" id="inputPassword" placeholder="8-12 Karakter" required>
                  <input type="checkbox" onclick="myFunction()" style="margin-top: 25px;"> Tampilkan Kata Sandi
                  <a href="#" style="margin-left: 80px;" data-bs-toggle="modal" data-bs-target="#modalLupa">Lupa kata sandi?</a>
                </div>
            </div>
            <div class="col" style="margin-left: 50px; padding-left: 20px;">
                <div class="d-grid gap-2 col-10 mx-auto">
                    <button class="btn btn-primary" type="submit" value="Submit">Masuk</button>
                </div>
            </div>
          </form>
          <div class="row">
            <p style="text-align: center; margin-left: 35px; margin-top: 20px; color: #828282;">Atau</p>
            <a href="google.com" style="margin-left: 160px;"><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/5/53/Google_%22G%22_Logo.svg/1200px-Google_%22G%22_Logo.svg.png" style="max-width: 4%; margin-right: 10px;" alt="">Masuk dengan akun google</a>
            <p style="text-align: center; margin-left: 35px; margin-top: 20px; color: #828282;">Belum punya akun? </p>
            <div class="col" style="margin-left: 50px; padding-left: 30px;">
              <div class="d-grid gap-2 col-10 mx-auto">
                  <button class="btn btn-outline-primary" type="button" data-bs-toggle="modal" data-bs-target="#modalDaftar">Daftar</button>
              </div>
            </div>
          </div>
        </div>
    </div>
-->
<div class="container" style="margin-top: 150px; margin-bottom: 50px;  padding-left: 150px; padding-right: 150px;">
  <div class="row">
    <div class="col">
      <img src="{{ asset('assets2/img/avatar.png')}}" alt="" style="object-fit: cover; width: 100%;">
    </div>
    <div class="col my-auto">
      <h2 class="display-7" style="text-align: center;">Masuk</h2>
      <form>
        <div class="form-floating mb-3">
          <input type="email" class="form-control" id="emailMasuk" placeholder="name@example.com" required>
          <label for="emailMasuk">Email address</label>
        </div>
        <div class="form-floating">
          <input type="password" class="form-control" id="passwordMasuk" placeholder="Password" required>
          <label for="passwordMasuk">Password</label>
        </div>
        <input type="checkbox" onclick="myFunction()" style="margin-top: 25px;" id="showPass">
        <label for="showPass">Tampilkan Kata Sandi</label>
        <a href="#" style="margin-left: 150px;" data-bs-toggle="modal" data-bs-target="#modalLupa">Lupa kata sandi?</a>
        <div class="col" >
          <div class="d-grid gap-2 col-6 mx-auto" style="margin-top: 20px;">
            <button class="btn btn-primary" type="submit" value="Submit" onclick="location.href='profil.html';" >Masuk</button>
          </div>
        </div>
      </form>
      <div class="row mx-auto">
        <p style="text-align: center; color: #828282; margin-top: 15px;">Atau</p>
      </div>
      <div class="row my-auto">
        <div style="text-align: center; padding-bottom:20px ;">
          <a href="https://google.com"><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/5/53/Google_%22G%22_Logo.svg/1200px-Google_%22G%22_Logo.svg.png" style="max-width: 4%; margin-right: 10px;" alt="">Masuk dengan akun Google</a>
        </div>
        <p style="text-align: center; color: #828282;">Belum punya akun? <a href="#" data-bs-toggle="modal" data-bs-target="#modalDaftar">Daftar di sini!</a> </p>
      </div>
    </div>
  </div>
</div>
<!-- Modal Lupa -->
<div class="modal fade" id="modalLupa" tabindex="-1" aria-labelledby="modalLupaLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalLupaLabel">Lupa Kata Sandi?</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <form>
      <div class="modal-body">
        <p style="text-align: center;">Masukkan email, atau nama pengguna dan kami akan mengirimi Anda tautan untuk kembali ke akun Anda</p>
        <input type="email" class="form-control" id="exampleFormControlInput1" placeholder="nama@domain.com / nama pengguna">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
        <button type="button" class="btn btn-primary">Kirim Tautan Masuk</button>
      </div>
    </form>
    </div>
  </div>
</div>

<!-- Modal Daftar-->
<div class="modal fade" id="modalDaftar" tabindex="-1" aria-labelledby="modalDaftarLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalDaftarLabel">Daftar</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <form>
          <div class="form-floating mb-3">
            <input type="text" class="form-control" id="floatingName" placeholder="Azizi Asadel" required>
            <label for="floatingName">Nama</label>
          </div>

          <div class="form-floating mb-3">
            <input type="email" class="form-control" id="floatingEmail" placeholder="contoh@domain.com">
            <label for="floatingEmail">Email</label>
          </div>

          <div class="form-floating mb-3">
            <input type="password" class="form-control" id="floatingPassword" placeholder="password">
            <label for="password">Kata Sandi</label>
          </div>

          <div class="form-floating mb-3">
            <input type="password" class="form-control" id="floatingPasswordRe" placeholder="password">
            <label for="passwordRe">Ulangi Kata Sandi</label>
          </div>
          <p style="text-align: center;">Dengan mendaftar, Anda menyetujui <b>Ketentuan</b>, <b> Kebijakan Data </b>, dan <b>Kebijakan Cookie</b> kami.</p>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
        <button type="button" class="btn btn-primary">Daftar</button>
      </div>
    </div>
  </div>
</div>

<footer>

</footer>
  <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
</body>
  <script type="module">
    import { Toast } from 'bootstrap.esm.min.js'
  
    Array.from(document.querySelectorAll('.toast'))
      .forEach(toastNode => new Toast(toastNode))
  </script>

  <script>
    function myFunction() {
      var x = document.getElementById("passwordMasuk");
      if (x.type === "password") {
        x.type = "text";
      } else {
        x.type = "password";
      }
    }
    </script>
</html>