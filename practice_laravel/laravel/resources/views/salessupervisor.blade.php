<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="{{ asset('assets2/img/icon.png')}}">
    <title>Cari Kerja</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Comfortaa&family=Outfit&family=Roboto:wght@500&display=swap" rel="stylesheet">
    <script crossorigin="anonymous" src="https://kit.fontawesome.com/e188a75150.js"></script>
  </head>
<style>
  .bg-nav{
    background-image: url("{{ asset('assets2/img/Rectangle.png')}}");
  }
  * {margin:0; padding: 0;}

body {font-family: Arial, Helvetica, sans-serif;}

/* Tombol Button Pesan */
#button {margin: 5% auto; width: 200px; text-align: center;}
#button a {
	width: 100px;
	height: 30px;
	vertical-align: middle;
	background-color: blue;
	color: #fff;
	text-decoration: none;
	padding: 10px;
	border-radius: 5px;
	border: 1px solid transparent;
}

/* Jendela Pop Up */
#popup {
	width: 100%;
	height: 100%;
	position: fixed;
	background: rgba(0,0,0,.7);
	top: 0;
	left: 0;
	z-index: 9999;
	visibility: hidden;
}

.window {
	width: 400px;
	height: 150px;
	background: #fff;
	border-radius: 10px;
	position: relative;
	padding: 10px;
	text-align: center;
	margin: 15% auto;
}
.window h2 {
	margin: 30px 0 0 0;
}
/* Button Close */
.close-button {
	width: 6%;
	height: 20%;
	line-height: 23px;
	background: #000;
	border-radius: 50%;
	border: 3px solid #fff;
	display: block;
	text-align: center;
	color: #fff;
	text-decoration: none;
	position: absolute;
	top: -10px;
	right: -10px;	
}

/* Memunculkan Jendela Pop Up*/
#popup:target {
	visibility: visible;
}
</style>

<body>
  <!-- Navigation -->

<nav class="navbar navbar-expand-lg navbar-light bg-secondary fixed-top " style="min-height: 50px;">
    <div class="container-fluid" style="padding: 15px 50px;">
      <a class="navbar-brand" href="#" style="color: white;">
        <img src="{{ asset('assets2/img/icon.png')}}" alt="..." height="36"> Cari Kerja
      </a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="nav ms-auto nav-pills">
          <li class="nav-item">
            <a class="nav-link" aria-current="page" href="<?= url('/'); ?>" style="color: white;">Utama</a>
          </li>
          <li class="nav-item">
            <a class="nav-link active" href="<?= url('lowongan'); ?>" aria-current="page" style="color: white; " >Lowongan</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?= url('teamkami'); ?>" aria-current="page" style="color: white;">Tentang</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?= url('faq'); ?>" aria-current="page" style="color: white;">Bantuan</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?= url('profil'); ?>" aria-current="page" style="color: white;">Profil</a>
          </li>
        </ul>
      </div>
    </div>
</nav>
    <div>  
        <div style="float:left; margin: 25px 50px 10px 10px;"> 
            <img src="{{ asset('assets2/img/sms.jpg')}}" width="200" height="200">
        </div>
        <div style="font-family: 'Roboto','sans-serif'; font-size: 18px;">
            <p style="width: 680px; padding: 75px 0 0px 0px; margin-top: 75px; font-size: x-large;"><b>Sales Supervisor</b></p>
            <p style="width: 680px; padding: 0px 0 0px 0px; margin-top: 0px; margin:0;">KSP Setia Multi Sarana</p>
            <p style="width: 680px; padding: 0px 0 0px 0px; margin-top: 0px; margin: 0;">Jakarta Selatan</p>
            <p style="width: 680px; padding: 0px 0 0px 0px; margin-top: 0px; margin: 0;">IDR 4.000.000</p>
            <p style="width: 680px; padding: 0px 0 0px 0px; margin-top: 0px; margin:0">Ditayangkan 4 Desember 2021</p>
        </div>
        <br>
        <hr>
        <div class="container-fluid">
          <div class="row">
            <div class="col-lg-8">
              <h5 style="margin: 10px 50px 0 0; padding-left: 30px; font-family: 'Roboto','sans-serif';">Deskripsi Pekerjaan</h5>
              <br>
              <h6 style="padding-left: 30px; font-family: 'Roboto','sans-serif';">Requirement</h6>
              <ul style="padding-left: 50px; font-family: 'Roboto','sans-serif'; font-size: 15px; ">
                <li>Usia maksimal 40 tahun</li>
                <li>Berdomisili di Kalideres, Cengkareng, Tangerang, dan Sekitarnya</li>
                <li>Memiliki motor sendiri dan SIM C</li>
                <li>Pendidikan miniman SMA/Sederajat</li>
                <li>Memiliki kemampuan dibidang pemasaran & negosiasi</li>
                <li>Aktif komunikasi dan koordinasi secara team dan individu</li>
              </ul>
              <h6 style="padding-left: 30px; font-family: 'Roboto','sans-serif';">Job Description</h6>
              <p style="font-size: 15px; margin: 10px 50px 0 0; text-align: justify; padding-left: 30px; font-family: 'Roboto','sans-serif';">
                Bicara tentang suatu pekerjaan tentu berbicara tentang tanggung jawab dan tugas dari pekerjaan itu sendiri.
                Sales supervisor perlu mengawasi kegiatan untuk meningkatkan penjualan. Mereka perlu mencapai KPI (Key Performance Indicator) 
                mereka dengan memotivasi tim sales selama periode penjualan. Fungsi mentor yang baik dengan cara memberikan feedback yang 
                berkelanjutan akan meningkatkan kinerja tim sales. Ketika tim sales tidak memenuhi targetnya, supervisor harus menemukan akar 
                permasalahan dan menerapkan strategi yang sesuai. Salah satu contoh adalah mengkoordinasikan training untuk meningkatkan 
                keterampilan para sales.</p>
            </div>
            <div class="col-lg-3" style="padding-left: 40px; margin-top: 0px; font-family: 'Roboto','sans-serif';">
              <p style="padding-top: 30px; margin: 0px;"><b>Tingkat Pekerjaan</b></p>
              <p style="font-size: 12px; margin: 0px 0px 5px 0px; color: darkgrey;"><b>Pegawai</b></p>
              <p style="margin: 5px 0 5px 0;"><b>Pengalaman</b></p>
              <p style="font-size: 12px; margin: 0 0 5px 0; color:darkgrey;"><b>Satu Tahun</b></p>
              <P style="margin: 5px 0 5px 0;"><b>Kualifikasi<b></P>
              <p style="font-size:12px;  margin: 0px; color: darkgrey;"><b>S1/D3, sertifikat profesional</b></p>
              <P style="margin: 5px 0 5px 0;"><b>Spesialisasi<b></P>
              <p style="font-size:12px;  margin: 0px; color: darkgrey;"><b>Management</b></p>
              <P style="margin: 5px 0 5px 0;"><b>Jenis Pekerjaan<b></P>
              <p style="font-size:12px;  margin: 0px; color: darkgrey;"><b>Full Time</b></p>
            </div>
          </div>
        </div>
        <div id="button"><a href="#popup">APPLY SEKARANG</a></div>
    
        <div id="popup">
          <div class="window">
              <a href="#" class="close-button" title="Close">X</a>
                <h2>Lamaran Anda Sudah Terkirim</h2>
            </div>
        </div>
    </div>
<footer>

</footer>
  <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script></body>
  <script type="module">
    import { Toast } from 'bootstrap.esm.min.js'
  
    Array.from(document.querySelectorAll('.toast'))
      .forEach(toastNode => new Toast(toastNode))
  </script>
</html>