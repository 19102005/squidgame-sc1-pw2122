<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="{{ asset('assets2/img/icon.png')}}">
    <title>Cari Kerja</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Comfortaa&family=Outfit&family=Roboto:wght@500&display=swap" rel="stylesheet">
  </head>
<style>
  .bg-nav{
    background-image: url("{{ asset('assets2/img/rectangle.png')}}");
  }
</style>

<body>
  <!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-light bg-secondary fixed-top " style="min-height: 50px;">
  <div class="container-fluid" style="padding: 15px 50px;">
    <a class="navbar-brand" href="#" style="color: white;">
      <img src="{{ asset('assets2/img/icon.png')}}" alt="..." height="36"> Cari Kerja
    </a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="nav ms-auto nav-pills">
        <li class="nav-item">
          <a class="nav-link active" aria-current="page" href="<?= url('/'); ?>">Utama</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="<?= url('lowongan'); ?>" aria-current="page" style="color: white; " >Lowongan</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="<?= url('teamkami'); ?>" aria-current="page" style="color: white;">Tentang</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="<?= url('faq'); ?>" aria-current="page" style="color: white;">Bantuan</a>
        </li>
        
        @auth
        <li class="nav-item">
          <form method="POST" action="{{ route('logout') }}">
            @csrf
            <a class="nav-link" href="{{route('logout')}}" aria-current="page" style="color: white;" onclick="event.preventDefault(); this.closest('form').submit();">Keluar</a>
          </form>
        </li>
        @endauth

        @guest
        <li class="nav-item">
          <a class="nav-link" href="<?= url('login'); ?>" aria-current="page" style="color: white;">Masuk</a>
        </li>
  
        @endguest
      </ul>
    </div>
  </div>
</nav>
  <!-- Navigation -->

  <!-- Page 1 -->
<div class="jumbotron">
  <div class="container mx-auto" style="width: 2500px; padding-top: 40px;">
    <div class="row" style="width: 1000px;">
      <div class="col-6 col-sm-8" style="padding-top: 100px;">
        <h1 class="display-2" style="font-family: 'Roboto', sans-serif;"><b>Cari Kerja</b></h1>
        <h3>Cara cepat untuk mencari kerja sesuai dengan keinginan kamu disini. Dapatkan rekomendasi pekerjaan yang sesuai dengan skill-mu disini dengan cepat dan efisien.</h3>
        <form>
        <div class="input-group input-group-lg" style="padding-top: 30px;">
          <input type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-lg" placeholder="Cari di sini">
          <button type="button" class="btn btn-primary btn-lg" style="padding: 15px 50px;">Cari</button>
        </div>
        </form>
      </div>
      <div class="col-sm-4"><img src="{{ asset('assets2/img/background.png')}}" alt="" style="width:700px;height:700px;"></div>
    </div>
  </div>
</div>
<div class="jumbotron">
  <div class="container my-auto mx-auto" style="padding-top: 30px; padding-bottom: 50px;">
    <div class="row">
      <div class="col">
        <img src="{{ asset('assets2/img/orang.png')}}" alt="">
      </div>
      <div class="col">
        <h1 class="display-2" style="font-family: 'Roboto', sans-serif; padding-left: 30px; padding-top: 150px ;"><b>Fitur</b></h1>
        <h3 class="display-6" style="padding-left: 30px; font-size: 32px;">Membantu kamu untuk mencari pekerjaan sesuai dengan kriteria kamu</h3><br>
        <div class="container rounded-3" style="background-color: #DEDEDE;" >
          <ul style="padding-left: 30px; font-size: 24px; padding-left: 90px; padding-top: 15px; padding-bottom: 15px;">
            <li>
              Gampang cari kerja
            </li>
            <li>
              Tersebar diseluruh Indonesia
            </li>
            <li>
              Cari kerja sesuai gaji yang kamu mau
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>
  <!-- Page 1 -->

  <!-- Page 2 -->
<div class="jumbotron" style="background-image: url('{{ asset('assets2/img/background2.png')}}');">
  <div class="container" style="width: 2500px; padding-top: 200px;">
    <div class="d-flex justify-content-center">
      <div class="container">
        <div class="row">
            <div class="col my-5" style="border-radius: 15px 50px; margin: 50px; border: 2px solid; padding-right: 10px; padding: 10px; box-shadow: 5px 10px #888888; background-color: white;">
              <h1 class="display-2" style="text-align: center; font-family: 'Roboto', sans-serif;"><b> >1000 </b></h1>
              <p style="text-align: center; font-size: 24px;">Lebih dari 1000 perusahaan terdaftar pada website ini</p>
            </div>
          <div class="col my-5" style="border-radius: 15px 50px; margin: 50px; border: 2px solid; padding: 10px; box-shadow: 5px 10px #888888; background-color: white;">
            <h1 class="display-2" style="text-align: center; font-family: 'Roboto', sans-serif;"><b> Lokasi </b></h1>
            <p style="text-align: center; font-size: 24px;">Lokasi yang tersebar di seluruh Indonesia</p>
          </div>
          <div class="col my-5" style="border-radius: 15px 50px; margin: 50px; border: 2px solid; padding: 10px; box-shadow: 5px 10px #888888; background-color: white;">
            <h1 class="display-2" style="text-align: center; font-family: 'Roboto', sans-serif;"><b> Gaji </b></h1>
            <p style="text-align: center; font-size: 24px;">Gaji yang sesuai dengan posisi dan kriteria kamu</p>
          </div>
        </div>
        <div class="row">
          <h2 style="text-align: center; padding: 100px; margin-bottom: 70px;">Dapatkan kemudahan dan efisien website pada device, agar mendapat pemberitahuan lebih cepat</h3>
          <img src="{{ asset('assets2/img/download.png')}}" alt="" style="max-width: 30%; margin-left: auto; margin-right: auto; margin-bottom: 50px;">
        </div>
      </div>
    </div>
  </div>  
</div>
  <!-- Page 2 -->

<footer>

</footer>
  <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script></body>
  <script type="module">
    import { Toast } from 'bootstrap.esm.min.js'
  
    Array.from(document.querySelectorAll('.toast'))
      .forEach(toastNode => new Toast(toastNode))
  </script>
</html>