<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="{{ asset('assets2/img/icon.png')}}">
    <title>Cari Kerja</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Comfortaa&family=Outfit&family=Roboto:wght@500&display=swap" rel="stylesheet">
    <script crossorigin="anonymous" src="https://kit.fontawesome.com/e188a75150.js"></script>
  </head>
<style>
  .bg-nav{
    background-image: url("{{ asset('assets2/img/Rectangle.png')}}");
  }
    
</style>

<body>
  <!-- Navigation -->

<nav class="navbar navbar-expand-lg navbar-light bg-secondary fixed-top " style="min-height: 50px;">
    <div class="container-fluid" style="padding: 15px 50px;">
      <a class="navbar-brand" href="#" style="color: white;">
        <img src="{{ asset('assets2/img/icon.png')}}" alt="..." height="36"> Cari Kerja
      </a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="nav ms-auto nav-pills">
          <li class="nav-item">
            <a class="nav-link" aria-current="page" href="<?= url('/'); ?>" style="color: white;">Utama</a>
          </li>
          <li class="nav-item">
            <a class="nav-link active" href="<?= url('lowongan'); ?>" aria-current="page" style="color: white; " >Lowongan</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?= url('teamkami'); ?>" aria-current="page" style="color: white;">Tentang</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?= url('faq'); ?>" aria-current="page" style="color: white;">Bantuan</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?= url('profil'); ?>" aria-current="page" style="color: white;">Profil</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>
  <h1 style="font-family: 'Roboto', sans-serif; margin-top: 125px; text-align: center;"><b>Keuangan & Ekonomi</b></h1> 
  <div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-lg-3" style="border: 2px solid darkgrey; margin: 10px 10px 10px 0px; border-radius: 20px; width: 200px; height: 220px; box-shadow: 2px 2px darkgrey;">
            <p style="text-align: center; font-size: small;" ><b>Accounting</b></p>
            <hr>
            <p style="text-align: justify; font-size: small;">Good reputation company at the national level as edutech platform, Incentive / Rewards Project, A great environment, culture and partners</p>
            <p style="text-align: right; font-size: small;"><button type="button" class="btn btn-primary">Detail View</button></p>
        </div>
        <div class="col-lg-3" style="border: 2px solid darkgrey; margin: 10px 10px 10px 10px; border-radius: 20px; width: 200px; height: 220px; box-shadow: 2px 2px darkgrey;" >
            <p style="text-align: center; font-size: small;" ><b>Staff Finance</b></p>
            <hr>
            <p style="text-align: justify; font-size: small;">Good reputation company at the national level as edutech platform, Incentive / Rewards Project, A great environment, culture and partners</p>
            <p style="text-align: right; font-size: small;"><button type="button" class="btn btn-primary">Detail View</button></p>
        </div>
        <div class="col-lg-3" style="border: 2px solid darkgrey; margin: 10px 10px 10px 10px; border-radius: 20px; width: 200px; height: 220px; box-shadow: 2px 2px darkgrey;">
            <p style="text-align: center; font-size: small;" ><b>Audit Staff</b></p>
            <hr>
            <p style="text-align: justify; font-size: small;">Good reputation company at the national level as edutech platform, Incentive / Rewards Project, A great environment, culture and partners</p>
            <p style="text-align: right; font-size: small;"><button type="button" class="btn btn-primary">Detail View</button></p>
        </div>
        <div class="col-lg-3" style="border: 2px solid darkgrey; margin: 10px 10px 10px 10px; border-radius: 20px; width: 200px; height: 220px; box-shadow: 2px 2px darkgrey;">
            <p style="text-align: center; font-size: small;" ><b>Tax Staff</b></p>
            <hr>
            <p style="text-align: justify; font-size: small;">Good reputation company at the national level as edutech platform, Incentive / Rewards Project, A great environment, culture and partners</p>
            <p style="text-align: right; font-size: small;"><button type="button" class="btn btn-primary">Detail View</button></p>
        </div>
    </div><br>
    <div class="row justify-content-center">
        <div class="col-lg-3" style="border: 2px solid darkgrey; margin: 10px 10px 10px 10px; border-radius: 20px; width: 200px; height: 220px; box-shadow: 2px 2px darkgrey;">
            <p style="text-align: center; font-size: small;" ><b>Administrator Staff</b></p>
            <hr>
            <p style="text-align: justify; font-size: small;">Good reputation company at the national level as edutech platform, Incentive / Rewards Project, A great environment, culture and partners</p>
            <p style="text-align: right; font-size: small;"><button type="button" class="btn btn-primary">Detail View</button></p>
        </div>
        <div class="col-lg-3" style="border: 2px solid darkgrey; margin: 10px 10px 10px 10px; border-radius: 20px; width: 200px; height: 220px; box-shadow: 2px 2px darkgrey;">
            <p style="text-align: center; font-size: small;" ><b>Finance Supervisor</b></p>
            <hr>
            <p style="text-align: justify; font-size: small;">Good reputation company at the national level as edutech platform, Incentive / Rewards Project, A great environment, culture and partners</p>
            <p style="text-align: right; font-size: small;"><button type="button" class="btn btn-primary">Detail View</button></p>
        </div>
        <div class="col-lg-3" style="border: 2px solid darkgrey; margin: 10px 10px 10px 10px; border-radius: 20px; width: 200px; height: 220px; box-shadow: 2px 2px darkgrey;">
            <p style="text-align: center; font-size: small;" ><b>Finance Staff</b></p>
            <hr>
            <p style="text-align: justify; font-size: small;">Good reputation company at the national level as edutech platform, Incentive / Rewards Project, A great environment, culture and partners</p>
            <p style="text-align: right; font-size: small;"><button type="button" class="btn btn-primary">Detail View</button></p>
        </div>
        <div class="col-lg-3" style="border: 2px solid darkgrey; margin: 10px 10px 10px 10px; border-radius: 20px; width: 200px; height: 220px; box-shadow: 2px 2px darkgrey;">
            <p style="text-align: center; font-size: small;" ><b>Accounting Staff</b></p>
            <hr>
            <p style="text-align: justify; font-size: small;">Good reputation company at the national level as edutech platform, Incentive / Rewards Project, A great environment, culture and partners</p>
            <p style="text-align: right; font-size: small;"><button type="button" class="btn btn-primary">Detail View</button></p>
        </div>
    </div>
  </div>
<footer>

</footer>
  <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script></body>
  <script type="module">
    import { Toast } from 'bootstrap.esm.min.js'
  
    Array.from(document.querySelectorAll('.toast'))
      .forEach(toastNode => new Toast(toastNode))
  </script>
</html>