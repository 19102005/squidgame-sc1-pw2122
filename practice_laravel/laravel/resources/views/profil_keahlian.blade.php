<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="{{ asset('assets2/img/icon.png')}}">
    <title>CariApa</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Comfortaa&family=Outfit&family=Roboto:wght@500&display=swap" rel="stylesheet">
    <script>
        // show the given page, hide the rest
        function show(elementID) {
        // find the requested page and alert if it's not found
            const ele = document.getElementById(elementID);
                if (!ele) {
                    alert("no such element");
                    return;
                 }

                // get all pages, loop through them and hide them
            const pages = document.getElementsByClassName('page');
                for (let i = 0; i < pages.length; i++) {
                    pages[i].style.display = 'none';
                }

                // then show the requested page
        ele.style.display = 'block';
        ele.classList.add('active');
    }
    </script>
    <style>
        .bg-nav{
          background-image: url("{{ asset('assets2/img/Rectangle.png')}}");
        }
        body {
  background-color: #fbfbfb;
}
@media (min-width: 991.98px) {
  main {
    padding:auto;
  }
}

/* Sidebar */
.sidebar {
  position: fixed;
  top: 0;
  bottom: 0;
  left: 0;
  padding: 58px 0 0; /* Height of navbar */
  box-shadow: 0 2px 5px 0 rgb(0 0 0 / 5%), 0 2px 10px 0 rgb(0 0 0 / 5%);
  width: 240px;
  z-index: 600;
}

@media (max-width: 991.98px) {
  .sidebar {
    width: 100%;
  }
}
.sidebar .active {
  border-radius: 5px;
  box-shadow: 0 2px 5px 0 rgb(0 0 0 / 16%), 0 2px 10px 0 rgb(0 0 0 / 12%);
}

.sidebar-sticky {
  position: relative;
  top: 0;
  height: calc(100vh - 48px);
  padding-top: 0.5rem;
  overflow-x: hidden;
  overflow-y: auto; /* Scrollable contents if viewport is shorter than content. */
}

    </style>
</head>


<body>
  <!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-light bg-secondary fixed-top " style="min-height: 50px;">
  <div class="container-fluid" style="padding: 15px 50px;">
    <a class="navbar-brand" href="#" style="color: white;">
      <img src="{{ asset('assets2/img/icon.png')}}" alt="..." height="36"> Cari Kerja
    </a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="nav ms-auto nav-pills">
        <li class="nav-item">
          <a class="nav-link" aria-current="page" style="color: white;" href="<?= url('/'); ?>">Utama</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="<?= url('lowongan'); ?>" aria-current="page" style="color: white; " >Lowongan</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="<?= url('teamkami'); ?>" aria-current="page" style="color: white;">Tentang</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="<?= url('faq'); ?>" aria-current="page" style="color: white;">Bantuan</a>
        </li>
        <li class="nav-item">
          <div class="dropdown">
            <button class="btn btn-primary dropdown-toggle active" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
              Profil
            </button>
            <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
              <li><a class="dropdown-item" href="<?= url('login'); ?>">Keluar</a></li>
              <li><a class="dropdown-item" href="#">Pengaturan</a></li>
            </ul>
          </div>
        </li>
      </ul>
    </div>
  </div>
</nav>

<!-- Navigation Side -->
<div class="container">
  <div class="row">
    <div class="col-sm-2">
      <nav id="sidebarMenu" class="collapse d-lg-block sidebar collapse bg-white" style="margin-top: 25px;">
        <div class="position-sticky">
          <div class="list-group list-group-flush mx-3 mt-4 my-auto">
    
            <a href="<?= url('profil'); ?>" class="list-group-item list-group-item-action py-2 ripple" aria-current="true">
              <i class="fas fa-fw me-3" href=""></i><span>Dashboard</span>
            </a>
            <a href="<?= url('profil_pengalaman'); ?>" class="list-group-item list-group-item-action py-2 ripple" aria-current="true" style="margin-top: 20px;">
              <i class="fas fa-fw me-3"></i><span>Pengalaman</span>
            </a>
            <a href="<?= url('profil_pendidikan'); ?>" class="list-group-item list-group-item-action py-2 ripple" aria-current="true" style="margin-top: 20px;">
              <i class="fas fa-fw me-3"></i><span>Pendidikan</span></a
            >
            <a href="<?= url('profil_keahlian'); ?>" class="list-group-item list-group-item-action py-2 ripple active" aria-current="true" style="margin-top: 20px;">
              <i class="fas fa-chart-line fa-fw me-3"></i><span>Keahlian</span></a
            >
            <a href="<?= url('profil_sertifikat'); ?>" class="list-group-item list-group-item-action py-2 ripple" aria-current="true" style="margin-top: 20px;">
              <i class="fas fa-chart-pie fa-fw me-3"></i><span>Sertifikat</span>
            </a>
          </div>
        </div>
      </nav>
    </div>

<!-- Main Layout -->
    <div class="col-sm-10">
      <div class="container pt-4">
        <div class="row">
          <h2 style="text-align: center; margin: 100px 0px 30px 0px; font-family: 'Roboto';">KEAHLIAN</h2>
            <div class="about">
              <div class="accordion" id="accordionExample">
                <div class="accordion-item">
                  <h2 class="accordion-header" id="headingOne">
                    <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                      CorelDraw
                    </button>
                  </h2>
                  <div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                    <div class="accordion-body">
                      <strong>Berpengalaman menggunakan CorelDraw</strong><br/>Saya hampir menggunakan coreldraw setiap project, saya sudah ahli menggunakan CorelDraw sejak SMP, dan kini
                      pada jenjang pendidikan sarjana CorelDraw menjadi bidang yang saya tekuni dimatakuliah
                    </div>
                  </div>
                </div>
                <div class="accordion-item">
                  <h2 class="accordion-header" id="headingTwo">
                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                      Riset
                    </button>
                  </h2>
                  <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
                    <div class="accordion-body">
                      <strong>Saya suka mereset dibidang Teknologi Informasi</strong><br/> Reset adalah kegiatan kesukaanku dispertiga malam, hampir setiap hari minggu saya melakukan seminar Tentang
                      riset saya dibidang teknologi informasi. 
                    </div>
                  </div>
                </div>
                <div class="accordion-item">
                  <h2 class="accordion-header" id="headingThree">
                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                      Desain
                    </button>
                  </h2>
                  <div id="collapseThree" class="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#accordionExample">
                    <div class="accordion-body">
                      <strong>Hobi Saya Mendesain</strong><br/> saya biasanya mendesain suatu prototype menggunakan figma, AdobeXD, namun dalam hal mendesain lebih sering menggunakan produk dari Adobe Creative
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col text-center" style="margin-top: 40px; margin-bottom: 40px;">
              <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
                Tambahkan Keahlian
              </button>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Tambah Keahlian</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <p style="color: gray;">Masukkan Keahlian Anda</p>
        <div class="mb-3">
          <label for="Input1" class="form-label">Keahlian</label>
          <input type="email" class="form-control" id="Input1" placeholder="example:memasak">
        </div>
        <div class="mb-3">
          <label for="exampleFormControlTextarea1" class="form-label">Deskripsi</label>
          <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div> 
</div>
<footer>

</footer>
  <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script></body>
  <script type="module">
    import { Toast } from 'bootstrap.esm.min.js'
  
    Array.from(document.querySelectorAll('.toast'))
      .forEach(toastNode => new Toast(toastNode))
  </script>
</html>