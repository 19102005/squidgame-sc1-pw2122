<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="{{ asset('assets2/img/icon.png')}}">
    <title>Cari Kerja</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Comfortaa&family=Outfit&family=Roboto:wght@500&display=swap" rel="stylesheet">
    <script>
        // show the given page, hide the rest
        function show(elementID) {
        // find the requested page and alert if it's not found
            const ele = document.getElementById(elementID);
                if (!ele) {
                    alert("no such element");
                    return;
                 }
 
                // get all pages, loop through them and hide them
            const pages = document.getElementsByClassName('page');
                for (let i = 0; i < pages.length; i++) {
                    pages[i].style.display = 'none';
                }

                // then show the requested page
        ele.style.display = 'block';
        ele.classList.add('active');
    }
    </script>
    <style>
        .bg-nav{
          background-image: url("{{ asset('assets2/img/Rectangle.png')}}");
        }
        body {
  background-color: #fbfbfb;
}
@media (min-width: 991.98px) {
  main {
    padding:auto;
  }
}

/* Sidebar */
.sidebar {
  position: fixed;
  top: 0;
  bottom: 0;
  left: 0;
  padding: 58px 0 0; /* Height of navbar */
  box-shadow: 0 2px 5px 0 rgb(0 0 0 / 5%), 0 2px 10px 0 rgb(0 0 0 / 5%);
  width: 240px;
  z-index: 600;
}

@media (max-width: 991.98px) {
  .sidebar {
    width: 100%;
  }
}
.sidebar .active {
  border-radius: 5px;
  box-shadow: 0 2px 5px 0 rgb(0 0 0 / 16%), 0 2px 10px 0 rgb(0 0 0 / 12%);
}

.sidebar-sticky {
  position: relative;
  top: 0;
  height: calc(100vh - 48px);
  padding-top: 0.5rem;
  overflow-x: hidden;
  overflow-y: auto; /* Scrollable contents if viewport is shorter than content. */
}

    </style>
</head>


<body>
  <!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-light bg-secondary fixed-top " style="min-height: 50px;">
  <div class="container-fluid" style="padding: 15px 50px;">
    <a class="navbar-brand" href="#" style="color: white;">
      <img src="{{ asset('assets2/img/icon.png')}}" alt="..." height="36"> Cari Kerja
    </a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="nav ms-auto nav-pills">
        <li class="nav-item">
          <a class="nav-link" aria-current="page" style="color: white;" href="<?= url('/'); ?>">Utama</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="<?= url('lowongan'); ?>" aria-current="page" style="color: white; " >Lowongan</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="<?= url('teamkami'); ?>" aria-current="page" style="color: white;">Tentang</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="<?= url('faq'); ?>" aria-current="page" style="color: white;">Bantuan</a>
        </li>
        <li class="nav-item">
          <div class="dropdown">
            <button class="btn btn-primary dropdown-toggle active" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
              Profil
            </button>
            <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
              <li><a class="dropdown-item" href="<?= url('login'); ?>">Keluar</a></li>
              <li><a class="dropdown-item" href="#">Pengaturan</a></li>
            </ul>
          </div>
        </li>
      </ul>
    </div>
  </div>
</nav>

<!-- Navigation Side -->
<div class="container">
  <div class="row">
    <div class="col-sm-2">
      <nav id="sidebarMenu" class="collapse d-lg-block sidebar collapse bg-white" style="margin-top: 25px;">
        <div class="position-sticky">
          <div class="list-group list-group-flush mx-3 mt-4 my-auto">
    
            <a href="<?= url('profil'); ?>" class="list-group-item list-group-item-action py-2 ripple" aria-current="true">
              <i class="fas fa-fw me-3" href=""></i><span>Dashboard</span>
            </a>
            <a href="<?= url('profil_pengalaman'); ?>" class="list-group-item list-group-item-action py-2 ripple" aria-current="true" style="margin-top: 20px;">
              <i class="fas fa-fw me-3"></i><span>Pengalaman</span>
            </a>
            <a href="<?= url('profil_pendidikan'); ?>" class="list-group-item list-group-item-action py-2 ripple" aria-current="true" style="margin-top: 20px;">
              <i class="fas fa-fw me-3"></i><span>Pendidikan</span></a
            >
            <a href="<?= url('profil_keahlian'); ?>" class="list-group-item list-group-item-action py-2 ripple" aria-current="true" style="margin-top: 20px;">
              <i class="fas fa-chart-line fa-fw me-3"></i><span>Keahlian</span></a
            >
            <a href="<?= url('profil_sertifikat'); ?>" class="list-group-item list-group-item-action py-2 ripple active" aria-current="true" style="margin-top: 20px;">
              <i class="fas fa-chart-pie fa-fw me-3"></i><span>Sertifikat</span>
            </a>
          </div>
        </div>
      </nav>
    </div>


<!--Main layout-->
    <div class="col-sm-10">
      <div class="container pt-4">
        <h2 style="text-align: center; margin: 100px 0px 30px 0px; font-family: 'Roboto';">SERTIFIKAT</h2>
     </div>
     <div class="shadow-lg p-3 mb-3 bg-white rounded" type="button" data-bs-toggle="modal" data-bs-target="#staticBackdrop">
      <div style="border: 5px solid white; background-color: white;margin-top: 15px;">
        <div>
          <img src="{{ asset('assets2/img/pendleton.png')}}" style="float: left; width: 70px; height: 90px; margin: 0px 20px 5px 5px;">
        </div> 
          <div style="font-size: medium; margin-left: 10px; font-family: 'Roboto';">
            <br<a><b class="fs-5">Sertifikat Kelulusan</b></a></br>
            <br<a>Pendleton Sixth form College</a></br>
            <br<a>May 2017</a></br>
            <br<a>Salford, United Kingdom</a></br>
          </div>
        </div>
    </div>
    <!-- Modal -->
  <div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog" style="max-width: 800px;">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="staticBackdropLabel">Sertifikat Kelulusan</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
          <img src="{{ asset('assets2/img/Sertifikat%Kelulusan.jpg')}}" style="max-width: 700px;">
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        </div>
    </div>
  </div>
</div>
    <div class="col text-center" style="margin-top: 40px; margin-bottom: 40px;">
      <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal" style="font-size: medium; font-family: 'Roboto';">Tambah Sertifikat</button>
    </div>
  </div>
  </div>
</div>

<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" style="max-width: 800px;">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">SERTIFIKAT</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
          <form>
            <div class="form-group">
              <label for="recipient-name" class="col-form-label">Universitas*</label>
              <input type="text" class="form-control" id="recipient-name">
            </div>
            <div class="form-group">
              <label for="recipient-name" class="col-form-label">Gelar*</label>
              <input type="text" class="form-control" id="recipient-name">
            </div>
            <div class="form-group">
              <label for="recipient-name" class="col-form-label">Bidang Studi*</label>
              <input type="text" class="form-control" id="recipient-name">
            </div>
            <div class="form-group">
              <label for="recipient-name" class="col-form-label">Tanggal Mulai*</label>
              <div class="container overflow-hidden">
                <div class="row gy-5">
                  <div class="col-6">
                    <div class="p-3">
                      <select class="form-select" aria-label="Disabled select example" disabled>
                        <option selected>Bulan</option>
                      </select>
                    </div>
                  </div>
                  <div class="col-6">
                    <div class="p-3">
                      <select class="form-select" aria-label="Disabled select example" disabled>
                        <option selected>Tahun</option>
                      </select>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="form-group">
              <label for="recipient-name" class="col-form-label">Tanggal Berakhir (atau perkiraan)*</label>
              <div class="container overflow-hidden">
                <div class="row gy-5">
                  <div class="col-6">
                    <div class="p-3">
                      <select class="form-select" aria-label="Disabled select example" disabled>
                        <option selected>Bulan</option>
                      </select>
                    </div>
                  </div>
                  <div class="col-6">
                    <div class="p-3">
                      <select class="form-select" aria-label="Disabled select example" disabled>
                        <option selected>Tahun</option>
                      </select>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="form-group">
              <label for="recipient-name" class="col-form-label">Nilai Kelulusan*</label>
              <input type="text" class="form-control" id="recipient-name">
            </div>
            <div class="form-group">
              <label for="message-text" class="col-form-label">Aktifitas dan Kegiatan Sosial</label>
              <textarea class="form-control" id="message-text"></textarea>
            </div>
          </form>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary">Save changes</button>
          </div>


<footer>

</footer>
  <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script></body>
  <script type="module">
    import { Toast } from 'bootstrap.esm.min.js'
  
    Array.from(document.querySelectorAll('.toast'))
      .forEach(toastNode => new Toast(toastNode))
  </script>
</html>