<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="{{ asset('assets2/img/icon.png')}}">
    <title>Cari Kerja</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Comfortaa&family=Outfit&family=Roboto:wght@500&display=swap" rel="stylesheet">
    <script crossorigin="anonymous" src="https://kit.fontawesome.com/e188a75150.js"></script>
  </head>
<style>
  .bg-nav{
    background-image: url("{{ asset('assets2/img/Rectangle.png')}}");
  }
    
</style>

<body>
  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-light bg-secondary fixed-top " style="min-height: 50px;">
    <div class="container-fluid" style="padding: 15px 50px;">
      <a class="navbar-brand" href="#" style="color: white;">
        <img src="{{ asset('assets2/img/icon.png')}}" alt="..." height="36"> Cari Kerja
      </a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="nav ms-auto nav-pills">
          <li class="nav-item">
            <a class="nav-link" aria-current="page" href="<?= url('/'); ?>" style="color: white;">Utama</a>
          </li>
          <li class="nav-item">
            <a class="nav-link active" href="<?= url('lowongan'); ?>" aria-current="page" style="color: white; " >Lowongan</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?= url('teamkami'); ?>" aria-current="page" style="color: white;">Tentang</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?= url('faq'); ?>" aria-current="page" style="color: white;">Bantuan</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?= url('profil'); ?>" aria-current="page" style="color: white;">Profil</a>
          </li>

          @auth
        <li class="nav-item">
          <form method="POST" action="{{ route('logout') }}">
            @csrf
            <a class="nav-link" href="{{route('logout')}}" aria-current="page" style="color: white;" onclick="event.preventDefault(); this.closest('form').submit();">Keluar</a>
          </form>
        </li>
        @endauth

       @guest
        <li class="nav-item">
          <a class="nav-link" href="<?= url('login'); ?>" aria-current="page" style="color: white;">Masuk</a>
        </li>
  
        @endguest
        </ul>
      </div>
    </div>
  </nav>
  <!-- Navigation -->
  <div class="container-fluid my-auto">
    <h1 style="font-family: 'Roboto', 'sans-serif'; margin: 125px 0px 40px 0; text-align: center;"><b>Lowongan</b></h1> 
    <div class="d-flex justify-content-around">
      <div class="row justify-content-center">
        <div class="col-lg-3" >
          <div class="contrainer" style="position: relative; text-align: center; color: white;">
            <p style="text-align: center; font-size: small; font-family: 'roboto','sans-serif';"><button type="button" onclick="location.href='<?= url('pemasaran'); ?>';" class="btn btn-primary" style="width: 200px; height: 100px; background-color: #1E90FF; color: white; border-color: #1E90FF; border-radius: 20px;"><i class="fa fa-bullhorn" aria-hidden="true" style="width: 20px; height: 20px;"></i>Pemasaran & Komunikasi</button></p>
            <div class="bottom-right" style="position: absolute; bottom: 8px; right: 16px; font-size: 10px;">139 lowongan tersedia</div>
          </div>
        </div>
        <div class="col-lg-3" >
          <div class="contrainer" style="position: relative; text-align: center; color: white;">
            <p style="text-align: center; font-size: small; font-family: 'roboto','sans-serif';"><button type="button" onclick="location.href='<?= url('pengembangan'); ?>';" class="btn btn-primary" style="width: 200px; height: 100px; background-color: #1E90FF; color: white; border-color: #1E90FF; border-radius: 20px;"><i class="fa fa-file-image-o" aria-hidden="true" style="width: 20px; height: 20px;"></i>Desain & Pengembangan</button></p>
            <div class="bottom-right" style="position: absolute; bottom: 8px; right: 16px; font-size: 10px;">139 lowongan tersedia</div>
          </div>
        </div>
        <div class="col-lg-3" >
          <div class="contrainer" style="position: relative; text-align: center; color: white;">
            <p style="text-align: center; font-size: small; font-family: 'roboto','sans-serif';"><button type="button" onclick="location.href='<?= url('sdm'); ?>';"  class="btn btn-primary" style="width: 200px; height: 100px; background-color: #1E90FF; color: white; border-color: #1E90FF; border-radius: 20px;"><i class="fa fa-users" aria-hidden="true" style="width: 20px; height: 20px;"></i>Sumber Daya Manusia</button></p>
            <div class="bottom-right" style="position: absolute; bottom: 8px; right: 16px; font-size: 10px;">139 lowongan tersedia</div>
          </div>
        </div>
        <div class="col-lg-3" >
          <div class="contrainer" style="position: relative; text-align: center; color: white;">
            <p style="text-align: center; font-size: small; font-family: 'roboto','sans-serif';"><button type="button" onclick="location.href='<?= url('keuangan'); ?>';" class="btn btn-primary" style="width: 200px; height: 100px; background-color: #1E90FF; color: white; border-color: #1E90FF; border-radius: 20px;"><i class="fa fa-money" aria-hidden="true" style="width: 20px; height: 20px; margin-right: 5px;"></i>Keuangan & Ekonomi</button></p>
            <div class="bottom-right" style="position: absolute; bottom: 8px; right: 16px; font-size: 10px;">139 lowongan tersedia</div>
          </div>
        </div>
      </div>
    </div>
    <div class="d-flex justify-content-around">
      <div class="row justify-content-center">
          <div class="col-lg-3" >
            <div class="contrainer" style="position: relative; text-align: center; color: white;">
              <p style="text-align: center; font-size: small; font-family: 'roboto','sans-serif';"><button type="button" onclick="location.href='<?= url('lembagahukum'); ?>';"  class="btn btn-primary" style="width: 200px; height: 100px; background-color: #1E90FF; color: white; border-color: #1E90FF; border-radius: 20px;"><i class="fa fa-balance-scale" aria-hidden="true" style="width: 20px; height: 20px; margin-right: 5px;"></i>Lembaga Hukum</button></p>
              <div class="bottom-right" style="position: absolute; bottom: 8px; right: 16px; font-size: 10px;">139 lowongan tersedia</div>
            </div>
          </div>
          <div class="col-lg-3" >
            <div class="contrainer" style="position: relative; text-align: center; color: white;">
              <p style="text-align: center; font-size: small; font-family: 'roboto','sans-serif';"><button type="button" onclick="location.href='<?= url('bisnis'); ?>';"  class="btn btn-primary" style="width: 200px; height: 100px; background-color: #1E90FF; color: white; border-color: #1E90FF; border-radius: 20px;"><i class="fa fa-line-chart" aria-hidden="true" style="width: 20px; height: 20px; margin-right: 5px;"></i>Bisnis & Konsultan</button></p>
              <div class="bottom-right" style="position: absolute; bottom: 8px; right: 16px; font-size: 10px;">139 lowongan tersedia</div>
            </div>
          </div>
          <div class="col-lg-3" >
            <div class="contrainer" style="position: relative; text-align: center; color: white;">
              <p style="text-align: center; font-size: small; font-family: 'roboto','sans-serif';"><button type="button" onclick="location.href='<?= url('layanan'); ?>';"  class="btn btn-primary" style="width: 200px; height: 100px; background-color: #1E90FF; color: white; border-color: #1E90FF; border-radius: 20px;"><i class="fa fa-handshake-o" aria-hidden="true" style="width: 20px; height: 20px; margin-right: 5px;"></i>Pelayan Pelanggan</button></p>
              <div class="bottom-right" style="position: absolute; bottom: 8px; right: 16px; font-size: 10px;">139 lowongan tersedia</div>
            </div>
          </div>
          <div class="col-lg-3" >
            <div class="contrainer" style="position: relative; text-align: center; color: white;">
              <p style="text-align: center; font-size: small; font-family: 'roboto','sans-serif';"><button type="button" onclick="location.href='<?= url('managemen'); ?>';" class="btn btn-primary" style="width: 200px; height: 100px; background-color: #1E90FF; color: white; border-color: #1E90FF; border-radius: 20px;"><i class="fa fa-file-text" aria-hidden="true" style="width: 20px; height: 20px; margin-right: 5px;"></i>Project Management</button></p>
              <div class="bottom-right" style="position: absolute; bottom: 8px; right: 16px; font-size: 10px;">139 lowongan tersedia</div>
            </div>
          </div>
      </div>
  </div>
</div>
<footer>

</footer>
  <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script></body>
  <script type="module">
    import { Toast } from 'bootstrap.esm.min.js'
  
    Array.from(document.querySelectorAll('.toast'))
      .forEach(toastNode => new Toast(toastNode))
  </script>
</html>