<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="{{ asset('assets2/img/icon.png')}}">
    <title>Cari Kerja</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Comfortaa&family=Outfit&family=Roboto:wght@500&display=swap" rel="stylesheet">
  </head>
<style>
  .bg-nav{
    background-image: url("{{ asset('assets2/img/Rectangle.png')}}");
  }
</style>

<body>
  <!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-light bg-secondary fixed-top " style="min-height: 50px;">
  <div class="container-fluid" style="padding: 15px 50px;">
    <a class="navbar-brand" href="#" style="color: white;">
      <img src="{{ asset('assets2/img/icon.png')}}" alt="..." height="36"> Cari Kerja
    </a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="nav ms-auto nav-pills">
          <li class="nav-item">
            <a class="nav-link" aria-current="page" href="<?= url('/'); ?>" style="color: white;">Utama</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?= url('lowongan'); ?>" aria-current="page" style="color: white; " >Lowongan</a>
          </li>
          <li class="nav-item">
            <a class="nav-link active" href="<?= url('teamkami'); ?>" aria-current="page" style="color: white;">Tentang</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?= url('faq'); ?>" aria-current="page" style="color: white;">Bantuan</a>
          </li>

          @auth
        <li class="nav-item">
          <form method="POST" action="{{ route('logout') }}">
            @csrf
            <a class="nav-link" href="{{route('logout')}}" aria-current="page" style="color: white;" onclick="event.preventDefault(); this.closest('form').submit();">Keluar</a>
          </form>
        </li>
        @endauth

        @guest
        <li class="nav-item">
          <a class="nav-link" href="<?= url('login'); ?>" aria-current="page" style="color: white;">Masuk</a>
        </li>
  
        @endguest
        </ul>
      </div>
  </div>
</nav>
<h1 style="font-family: 'Roboto', sans-serif; margin-top: 125px; margin-bottom: 30px; text-align: center;"><b>Team Kami</b></h1> 
<div class="container-fluid">
  <div class="row">
    <div class="d-flex justify-content-center">
      <div class="col-lg-3" >
        <div class="card" style="width: 18rem; background-color: cornflowerblue; border-radius: 20px;">
            <img src="{{ asset('assets2/img/IMG_2186.JPG')}}" class="card-img-top" style="border-radius: 20px;">
            <div class="card-body">
              <p class="card-text" style="text-align: center; color: white; font-family: 'Roboto', 'sans-serif';">Julian Saputra<br/>19102008<br/>@juximoff</p>
            </div>
        </div>
      </div>
      <div class="col-lg-3">
        <div class="card" style="width: 18rem; background-color: cornflowerblue; border-radius: 20px;">
            <img src="{{ asset('assets2/img/IMG_20210922_110829.jpg')}}" class="card-img-top" style="border-radius: 20px;">
            <div class="card-body">
              <p class="card-text"  style="text-align: center; color: white; font-family: 'Roboto', 'sans-serif';">Imada Ramadhanti<br/>1910203<br/>@Imadarmd</p>
            </div>
        </div>
      </div>
      <div class="col-lg-3">
        <div class="card" style="width: 18rem; background-color: cornflowerblue; border-radius: 20px;">
            <img src="{{ asset('assets2/img/IMG_8118.jpg')}}" class="card-img-top" style="border-radius: 20px;">
            <div class="card-body">
              <p class="card-text"  style="text-align: center; color: white; font-family: 'Roboto', 'sans-serif';">Ibrohim Huzaimi<br/>19102005<br>@boimvincenia</p>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>

    <footer>

</footer>
  <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script></body>
  <script type="module">
    import { Toast } from 'bootstrap.esm.min.js'
  
    Array.from(document.querySelectorAll('.toast'))
      .forEach(toastNode => new Toast(toastNode))
  </script>
</html>